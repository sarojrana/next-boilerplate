import React from 'react';
import Head from 'next/head';
import { connect } from 'react-redux';

import AppState from '@project/interfaces/states/AppState';
import { incrementCounter, decrementCounter } from '@project/redux/actions/user';

interface Props {
  counter: number;
}

type InjectedProps = Props & typeof mapDispatchToProps;
class App extends React.Component<InjectedProps> {
  constructor(props: InjectedProps) {
    super(props);
  }

  render() {
    return (
      <div className="container">
        <Head>
          <title>Create Next App</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main>
          <h1 className="title">Welcome to Next React App. Time Counter</h1>
          <div>
            <button className="btn" onClick={this.props.incrementCounter}>
              Increment
            </button>
            <button onClick={this.props.decrementCounter}>Decrement</button>
            <h1>{this.props.counter}</h1>
          </div>
        </main>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState) => ({
  counter: state.data.user.counter
});

const mapDispatchToProps = {
  incrementCounter,
  decrementCounter
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
