import { NextApiRequest, NextApiResponse } from 'next';

import { getAllHello } from '@project/services/hello';

export default (req: NextApiRequest, res: NextApiResponse): void => {
  const val = getAllHello();

  res.statusCode = 200;
  res.json({ data: val });
};
