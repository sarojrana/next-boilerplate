import React from 'react';
import PropTypes from 'prop-types';

import { withTranslation } from '../i18n';

const Login = ({ t }) => {

  return (
    <div>
      <h3> {t('h1')} </h3>
    </div>
  );
};

Login.getInitialProps = async () => ({
  namespacesRequired: ['common'],
})

Login.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation('common')(Login);
