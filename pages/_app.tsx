import React from 'react';
import { AppProps } from 'next/app';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';

import '@project/public/sass/main.scss';
import { appWithTranslation } from '../i18n';
import { useStore } from '@project/redux/store';

const WebApp = ({ Component, pageProps }: AppProps): JSX.Element => {
  const { initialReduxState } = pageProps;

  const store = useStore(initialReduxState);
  const persistor = persistStore(store);

  return (
    <Provider store={store}>
      <PersistGate loading={<Component {...pageProps} />} persistor={persistor}>
        <Component {...pageProps} />
      </PersistGate>
    </Provider>
  );
};

WebApp.getInitialProps = async ({ Component, ctx }) => {
  let pageProps = {};

  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx);
  }

  return { pageProps };
};

export default appWithTranslation(WebApp);
