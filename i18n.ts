import NextI18Next from 'next-i18next';

const nextI18n = new NextI18Next({
  defaultLanguage: 'en',
  otherLanguages: ['de'],
  strictMode: false,
});

nextI18n.i18n.languages = ['en', 'ne'];

export const useTranslation = nextI18n.useTranslation;
export const withTranslation = nextI18n.withTranslation;
export const appWithTranslation = nextI18n.appWithTranslation;
export const i18n = nextI18n;
