import React from 'react';

export interface Props {
  label: string;
  href?: string;
  onClick?(): void;
  isDisabled?: boolean;
  type?: 'submit' | 'reset' | 'button';
  icon?: string;
  classType: string;
}

const handleClick = () => {
  const { onClick } = this.props;
  onClick();
};

const Button = (props: Props): JSX.Element => {
  const { label, isDisabled, onClick, type, classType } = props;

  return (
    <button className={classType} onClick={onClick ? handleClick : undefined} disabled={isDisabled} type={type}>
      {label}
    </button>
  );
};

export default Button;
