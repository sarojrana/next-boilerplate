interface Hello {
  id: number;
  title: string;
  description: string;
}

export const getAllHello = (): Array<Hello> => {
  return [
    {
      id: 1,
      title: 'Namaste',
      description: 'Nepali hello'
    }
  ];
};
