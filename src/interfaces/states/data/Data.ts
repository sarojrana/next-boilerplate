import UserState from './User';

interface Data {
  readonly user: UserState;
}

export default Data;
