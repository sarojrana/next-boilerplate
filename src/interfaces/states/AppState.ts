import DataState from './data/Data';

interface AppState {
  // readonly ui: any;
  readonly data: DataState;
}

export default AppState;
