import { Store, Dispatch } from 'redux';
import { AppInitialProps } from 'next/app';

interface AppStore extends Store {
  dispatch: Dispatch;
}

export interface AppWithStore extends AppInitialProps {
  store: AppStore;
}
