import { UserActions } from '@project/redux/actions/user';

type AppActions = UserActions;

export default AppActions;
