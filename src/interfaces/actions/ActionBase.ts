export interface Action<T, M = unknown> {
  type: T;
  meta: M;
}

export interface ActionWithPayload<T, P, M = unknown> extends Action<T, M> {
  payload: P;
}

export interface ActionWithError<T, P, M = unknown> extends ActionWithPayload<T, P, M> {
  error: true;
}
