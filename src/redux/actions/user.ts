import { Action } from '@project/interfaces/actions/ActionBase';

export const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
export type INCREMENT_COUNTER = typeof INCREMENT_COUNTER;

export const DECREMENT_COUNTER = 'DECREMENT_COUNTER';
export type DECREMENT_COUNTER = typeof DECREMENT_COUNTER;

// Types for actions
export type IncrementCounter = Action<INCREMENT_COUNTER>;
export type DecrementCounter = Action<DECREMENT_COUNTER>;

export type UserActions = IncrementCounter | DecrementCounter;

export const incrementCounter = () => ({
  type: INCREMENT_COUNTER
});

export const decrementCounter = () => ({
  type: DECREMENT_COUNTER
});
