import { combineReducers } from 'redux';

// import uiReducer from './ui';
import dataReducer from './data';

export default combineReducers({
  // ui: uiReducer,
  data: dataReducer
});
