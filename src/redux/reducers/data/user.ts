import UserState from '@project/interfaces/states/data/User';
import AppActions from '@project/interfaces/actions/AppActions';
import { INCREMENT_COUNTER, DECREMENT_COUNTER } from '@project/redux/actions/user';

export const INITIAL_STATE: UserState = {
  counter: 0
};

const userReducer = (state = INITIAL_STATE, action: AppActions): UserState => {
  switch (action.type) {
    case INCREMENT_COUNTER:
      return { ...state, counter: state.counter + 1 };
    case DECREMENT_COUNTER:
      return { ...state, counter: state.counter - 1 };
    default:
      return { ...state };
  }
};

export default userReducer;
