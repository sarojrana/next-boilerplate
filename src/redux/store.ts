import thunk from 'redux-thunk';
import storage from 'redux-persist/lib/storage';
import { createStore, applyMiddleware } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';

import rootReducer from './reducers';
import { useMemo } from 'react';

let store;

const persistConfig = {
  key: 'primary',
  storage
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const makeStore = (initialState = {}) =>
  createStore(persistedReducer, initialState, composeWithDevTools(applyMiddleware(thunk)));

export const initializeStore = (preloadedState = {}): typeof store => {
  let _store = store ?? makeStore(preloadedState);

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = makeStore({
      ...store.getState(),
      ...preloadedState
    });
    // Reset the current store
    store = undefined;
  }

  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') {
    _store._PERSISTOR = persistStore(_store);

    return _store;
  }
  // Create the store once in the client
  if (!store) store = _store;

  return _store;
};

export const useStore = (initialState = {}) => useMemo(() => initializeStore(initialState), [initialState]);
